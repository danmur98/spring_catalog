package com.epam.rd.autotasks.springemployeecatalog;

import com.epam.rd.autotasks.springemployeecatalog.domain.Department;
import com.epam.rd.autotasks.springemployeecatalog.domain.Employee;
import com.epam.rd.autotasks.springemployeecatalog.domain.FullName;
import com.epam.rd.autotasks.springemployeecatalog.domain.Position;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;


@SpringBootApplication
@RestController
public class MvcApplication {
    private final List<Employee> employees;

    public MvcApplication() {
        employees = new ArrayList<>();
        employees.add(new Employee(1L, new FullName("John", "Doe", ""), Position.MANAGER, null, null, null, null));
        employees.add(new Employee(2L, new FullName("Jane", "Smith", ""), Position.CLERK, null, null, employees.get(0), null));
    }


    public static void main(String[] args) {
        SpringApplication.run(MvcApplication.class, args);
    }

    @GetMapping("/employees")
    public ResponseEntity<List<Employee>> getAllEmployees(
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "10") int size,
            @RequestParam(defaultValue = "lastName") String sort
    ) {
        List<Employee> pagedEmployees = employees.stream()
                .sorted(getEmployeeComparator(sort))
                .skip(page * size)
                .limit(size)
                .collect(Collectors.toList());

        return ResponseEntity.ok(pagedEmployees);
    }

    @GetMapping("/employees/{employee_id}")
    public ResponseEntity<Employee> getEmployeeById(
            @PathVariable("employee_id") Long employeeId,
            @RequestParam(defaultValue = "false") boolean full_chain
    ) {
        Employee employee = employees.stream()
                .filter(emp -> emp.getId().equals(employeeId))
                .findFirst()
                .orElse(null);

        if (employee == null) {
            return ResponseEntity.notFound().build();
        }

        if (full_chain) {
            List<Employee> chain = new ArrayList<>();
            Employee currentEmployee = employee;

            while (currentEmployee != null) {
                chain.add(currentEmployee);
                currentEmployee = currentEmployee.getManager();
            }

            employee.setManagerChain(chain);
        }

        return ResponseEntity.ok(employee);
    }

    @GetMapping("/employees/by_manager/{managerId}")
    public ResponseEntity<List<Employee>> getEmployeesByManagerId(
            @PathVariable("managerId") Long managerId,
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "10") int size,
            @RequestParam(defaultValue = "lastName") String sort
    ) {
        List<Employee> subordinates = employees.stream()
                .filter(emp -> emp.getManager() != null && emp.getManager().getId().equals(managerId))
                .sorted(getEmployeeComparator(sort))
                .skip(page * size)
                .limit(size)
                .collect(Collectors.toList());

        return ResponseEntity.ok(subordinates);
    }

    @GetMapping("/employees/by_department/{departmentIdOrName}")
    public ResponseEntity<List<Employee>> getEmployeesByDepartment(
            @PathVariable("departmentIdOrName") String departmentIdOrName,
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "10") int size,
            @RequestParam(defaultValue = "lastName") String sort
    ) {
        List<Employee> employeesInDepartment = employees.stream()
                .filter(emp -> isEmployeeInDepartment(emp, departmentIdOrName))
                .sorted(getEmployeeComparator(sort))
                .skip(page * size)
                .limit(size)
                .collect(Collectors.toList());

        return ResponseEntity.ok(employeesInDepartment);
    }

    private Comparator<Employee> getEmployeeComparator(String sort) {
        switch (sort) {
            case "firstName":
                return Comparator.comparing(e -> e.getFullName().getFirstName());
            case "lastName":
                return Comparator.comparing(e -> e.getFullName().getLastName());
            case "position":
                return Comparator.comparing(e -> e.getPosition().toString());
            default:
                throw new IllegalArgumentException("Invalid sort parameter: " + sort);
        }
    }

    private boolean isEmployeeInDepartment(Employee employee, String departmentIdOrName) {
        if (employee.getDepartment() != null) {
            Department department = employee.getDepartment();
            return department.getName().equalsIgnoreCase(departmentIdOrName) ||
                    department.getId().equals(Long.parseLong(departmentIdOrName));
        }
        return false;
    }
}


